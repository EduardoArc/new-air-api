const http = require("http");
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const routes = require("./src/routes");
const createRoles = require("./src/libs/initialSetup");

const socketUtils = require("./utils/socketUtils");

//process.env.TZ = "America/Santiago";
const app = express();
const server = http.createServer(app);
const io = socketUtils.sio(server);
socketUtils.connection(io);
global.io = io;

const socketIOMiddleware = (req, res, next) => {
  req.io = io;
  next();
};

app.use(cors());

app.use("/api/v1/hello", socketIOMiddleware, (req, res) => {
  req.io.emit("message", `Hello, ${req.originalUrl}`);
  res.send("Hola mundo");
});

//Crea roles
createRoles();

mongoose.Promise = global.Promise;
mongoose.connect(
  "mongodb+srv://administrator:adminadmin@cluster0.slqeo.mongodb.net/nuevo-aire-api?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
  }
);

//habilitar body parser
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//habilitar cors

app.use("/", routes());

const puerto = process.env.PORT || 3000;

server.listen(puerto, function () {
  console.log("Servidor web en ejecución: puerto " + puerto);
});
