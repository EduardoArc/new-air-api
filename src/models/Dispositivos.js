const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const dispositivosSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
  },
  latitud: {
    type: String,
    trim: true,
  },
  longitud: {
    type: String,
    trim: true,
  },
  token: {
    type: String,
    trim: true,
  },
  grupo: {
    type: mongoose.Types.ObjectId,
    ref: "grupos",
  },

  estado: {
    type: Number,
    default: 1,
  },
});

module.exports = mongoose.model("dispositivos", dispositivosSchema);
