const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");

const usersSchema = new Schema({
    nombre: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        unique: true,
        trim: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    temp_password: {
        type: String,
        required: true,
    },
    grupos: [{
        type: mongoose.Types.ObjectId,
        ref: 'grupos'
    }],
    organizaciones: [{
        type: mongoose.Types.ObjectId,
        ref: 'organizaciones'
    }],

    roles: [{
        ref: 'roles',
        type: mongoose.Types.ObjectId,
    }],


    estado: {
        type: Number,
        default: 1
    }
  

});

usersSchema.statics.encriptPassword = async (password) =>{
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
 }
 
 usersSchema.statics.comparePassword = async (password, receivedPassword) => {
     return await bcrypt.compare(password, receivedPassword);
 }

module.exports = mongoose.model("users", usersSchema);