const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const eventosSchema = new Schema({
    dispositivo: {
        type: mongoose.Types.ObjectId,
        ref: "dispositivos",
      },
    parametro: {
        type: String,
        trim: true,
    },

    alerta: {
        type: Boolean,
        default: false,
        
    },
    prioridad: {
        type: Number,
        default: 0,

    },
    titulo:{
        type: String,
        trim : true
    },
    mensaje: {
        type: String,
        trim : true
    }

   

}, {
    timestamps: true
});

module.exports = mongoose.model("eventos", eventosSchema);