const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const organizacionesSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
       // unique: true

    },

    descripcion: {
        type: String,
        trim: true,
       

    },
    grupos: [{
        type: mongoose.Types.ObjectId,
        ref: 'grupos'
    }],
    users: [{
        type: mongoose.Types.ObjectId,
        ref: 'users'
    }],
    estado: {
        type: Number,
        default: 1
    }

   

}, {
    timestamps: true
});

module.exports = mongoose.model("organizaciones", organizacionesSchema);