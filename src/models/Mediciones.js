const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const medicionesSchema = new Schema(
  {
    rzero: {
      type: Number,
      trim: true,
    },
    pm10: {
      type: Number,
      trim: true,
    },
    pm25: {
      type: Number,
      trim: true,
    },
    co2: {
      type: Number,
      trim: true,
    },
    temperatura: {
      type: Number,
      trim: true,
    },
    humedad: {
      type: Number,
      trim: true,
    },
    dispositivo: {
      type: mongoose.Types.ObjectId,
      ref: "dispositivos",
    },
    estado: {
      type: Number,
      default: 1,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("mediciones", medicionesSchema);
