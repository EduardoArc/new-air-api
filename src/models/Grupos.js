const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const gruposSchema = new Schema({
    nombre: {
        type: String,
        trim: true

    },
    organizacion: { 
        type: mongoose.Types.ObjectId,
        ref: 'organizaciones'
    },

    dispositivos: [{ 
        type: mongoose.Types.ObjectId,
        ref: 'dispositivos'
    }],
    
    users: [{ 
        type: mongoose.Types.ObjectId,
        ref: 'users'
    }],
    
    estado: {
        type: Number,
        default: 1
    }
  

});

module.exports = mongoose.model("grupos", gruposSchema);