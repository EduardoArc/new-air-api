const express = require("express");

const router = express.Router();

const userController = require("../controllers/usersController");
const dispositivosController = require("../controllers/dispositivosControllers");
const grupoController = require("../controllers/gruposController");
const organizacionController = require("../controllers/organizacionController");
const authController = require("../controllers/authController");
const medicionController = require("../controllers/medicionController");
const eventoController = require("../controllers/eventoController");
const socketController = require("../controllers/socketController");

const authJwt = require("../middlewares/authJwt");
const verifySignup = require("../middlewares/verifySignup");
const verifyMedicion = require("../middlewares/verifyMedicion");

module.exports = function () {
  router.get("/", (req, res) => {
    // let socket = io();
    res.send("nuevo aire Desplegado!");
  });

  /*/------ESTABLECIMIENTOS-------/*/

  //Listado de organizaciones
  router.get("/organizaciones", organizacionController.list);

  //Agregar nueva organizacion
  router.post(
    "/organizaciones",
    [authJwt.verifyToken],
    organizacionController.add
  ); // [ authJwt.verifyToken , authJwt.isAdmin],

  //Muestra una organizacion | get: :id
  router.get("/organizaciones/:id", organizacionController.show);

  router.get("/socket", socketController.sendMessage);

  //editar organización /organizaciones/:id
  router.put("/organizaciones/:id", organizacionController.update);

  //Elimina una organizacion
  router.delete("/organizaciones/:id", organizacionController.delete);

  //Activa una organizacion
  router.post("/organizaciones/activa/:id", organizacionController.activa);

  // Agregar grupos a organizacion
  router.post("/organizacion/grupos", organizacionController.addGrupos);

  //Quitar organizacion a usuario
  router.post(
    "/organizacion/grupos/delete",
    organizacionController.deleteGrupos
  );

  /*/------DISPOSITIVOS 🗼-------/*/

  //Agrega dispositivos | post: /dispositivos
  router.post("/dispositivos", dispositivosController.add);

  //Obtiene listado de dispositivos | get: dispositivos
  router.get(
    "/dispositivos",
    [authJwt.verifyToken],
    dispositivosController.list
  );

  //Obtiene el resumen de la medición | get: dispisitivos
  router.get(
    "/dispositivos/medicion",
    authJwt.verifyToken,
    dispositivosController.listMedicion
  );

  //Muestra un dispositivo | get: /customers/:id
  router.get("/dispositivos/:id", dispositivosController.show);

  //Edita un dispositivo
  router.put("/dispositivos/:id", dispositivosController.update);

  //Elimina un dispositivo
  router.delete("/dispositivos/:id", dispositivosController.delete);

  router.post("/dispositivos/activa/:id", dispositivosController.activa);

  /*/------MEDICIONES 📈-------/*/

  //Agrega medicion | post: /mediciones
  router.post(
    "/mediciones",
    authJwt.verifyTokenDispositivo,
    medicionController.add
  );

  //Obtiene listado de mediciones ente las fechas solucitadas| get: mediciones /:start /:end
  router.get("/mediciones/entre/:dispositivo_id", medicionController.entre);

  //Obtiene la medición más reciente de un dispositivo
  router.get(
    "/mediciones/ultima/:dispositivo_id",
    medicionController.ultimaMedicion
  );

  //Obtiene promedio de mediciones entre las fechas indicadas| get: mediciones /:start /:end
  router.get(
    "/mediciones/promdia/:dispositivo_id",
    medicionController.promediodiario
  );

  //muestra los promedios de los parámetros de mediciones de los 7 días anteriores | /:dispositivo_id
  router.get(
    "/mediciones/anteriores/:dispositivo_id",
    medicionController.diasanteriores
  );

  //Muestra una medicion | get: /mediciones/:id
  router.get("/mediciones/:id", medicionController.show);

  //Elimina una medicion
  router.delete("/mediciones/:id", medicionController.delete);

  //Muestra mediciones según dispositivo
  router.get(
    "/mediciones/promedio/semanal/:dispositivo_id",
    medicionController.weekendAverageByDevice
  );

  //Muestra ultimas 5 mediciones según dispositivo
  router.get(
    "/mediciones/ultimas/:dispositivo_id",
    medicionController.lastFiveData
  );

  /*/------EVENTOS ❗❗❗-------/*/

  //Obtiene listado de mediciones| get: mediciones
  router.get("/eventos", eventoController.list);

  //Muestra mediciones según dispositivo | get: /customers/:id
  router.get(
    "/eventos/dispositivo/:dispositivo_id",
    verifyMedicion.checkDispositivo,
    eventoController.listByDispositivo
  );

  /*/------USERS 🙍‍♂️-------/*/

  //post: /users
  router.post(
    "/usuarios",
    [authJwt.verifyToken, verifySignup.checkRolesExisted],
    userController.add
  ); //  [ authJwt.verifyToken , authJwt.isAdmin, verifySignup.checkRolesExisted]

  router.get("/verify", authJwt.verifyExpire);
  //get: users
  router.get("/usuarios", [authJwt.verifyToken], userController.list);

  //get: alumnos según la organización de su monitor
  router.get("/alumnos", [authJwt.verifyToken], userController.getAlumnos);

  //get: users :id
  router.get("/usuarios/:id", [authJwt.verifyToken], userController.show);

  //Edita un user
  router.put(
    "/usuarios/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    userController.update
  );

  //eliminar un user
  router.delete(
    "/usuarios/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    userController.delete
  );

  //activa un user
  router.post("/usuarios/activa/:id", userController.activa);

  // Agregar organizacion a usuario
  router.post("/usuarios/organizaciones", userController.addOrganizacion);

  // Obtener grupos de usuario
  router.get("/usuarios/grupos/:id", userController.getGrupos);

  //Objetener usuarios por grupo
  router.get("/usuarios/grupo/:id", userController.getAlumnosPorGrupo);

  //Quitar organizacion a usuario
  router.post(
    "/usuarios/organizaciones/delete",
    userController.deleteOrganiacion
  );

  //Cambia password del usuario
  router.post("/usuarios/chpass", userController.changePassword);

  //get: dispositivos del usuario :id
  router.get(
    "/usuarios/dispositivos/:id",
    [authJwt.verifyToken],
    userController.usuarioDispositivos
  );

  /*/------GRUPOS 👨‍👩‍👦-------/*/

  //Agrega grupo
  router.post("/grupos", grupoController.add);

  //Agrega dispositivo a un grupo
  router.post("/grupos/dispositivos", grupoController.addDevice);

  //Eliminar dispositivo de un grupo
  router.post("/grupos/dispositivos/delete", grupoController.deleteDevice);

  //listar grupos
  router.get("/grupos", grupoController.list);

  //show /grupo :id
  router.get("/grupos/:id", grupoController.show);

  //Editar /grupo :id
  router.put("/grupos/:id", grupoController.update); //👈

  //TODO: eliminar grupo :id
  router.delete("/grupos/:id", grupoController.delete);

  //TODO: habilita grupo :id
  router.post("/grupos/activa/:id", grupoController.activa);

  router.post("/signin", authController.signIn);

  router.post("/signup", authController.signUp);

  return router;
};
