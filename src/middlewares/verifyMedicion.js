const jwt = require("jsonwebtoken");
const Role = require("../models/Role");
const Users = require("../models/Users");

//Verificar si el usuario pertenece a la organizacion o el grupo del dispositivo
exports.checkDispositivo = async (req, res, next) => {
  //capturar dispositivo

  try {
    const  dispositivo  = req.params.dispositivo_id;



    const token = req.headers["x-access-token"];

    const decoded = jwt.verify(token, "aire-api");
    const userId = decoded.id;

    const user = await Users.findById(userId).populate({path : 'organizaciones', populate : {path : "grupos"}});

    const organizaciones = user.organizaciones;
    const grupos = [];


    organizaciones.forEach(organizacion => {
      organizacion.grupos.forEach(grupo =>{
        grupos.push(grupo);
      })
    });

    let dispositivos = [];

    grupos.forEach(grupo=>{
      grupo.dispositivos.forEach(disp=>{
        dispositivos.push(disp)
      })
    })
   
    if(dispositivos.includes(dispositivo)){
      next();
      return;
    }
  //return res.status(404).json({dispositivos});


     return res.status(403).json({message:  "Dispositivo no asociado"});
     
  } catch (error) {
    return res.status(401).json({error : error})
  }


};
