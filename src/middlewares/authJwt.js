const jwt = require("jsonwebtoken");
const Users = require("../models/Users");
const Roles = require("../models/Role");
const Dispositivos = require("../models/Dispositivos");

exports.verifyExpire = async (req, res) => {
  try {
    console.log("asdads");
    const token = req.headers["x-access-token"];
    console.log("token done", token);

    if (!token) {
      return res.status(403).json({ message: "Sin token de acceso" });
    }

    const decoded = jwt.verify(token, "aire-api");

    let expired = false;

    if (Date.now() >= decoded.exp * 1000) {
      expired = true;
    } else {
      expired = false;
    }
    // req.userId = decoded.id;
    // return res.status(200).json(decoded.exp * 1000);
    return res.status(200).json(expired);

    /*
    const user = await Users.findById(req.userId, { password: 0 });
    // console.log(user);
    if (!user) {
      return res.status(404).json({ message: "usuario no encontrado" });
    } */
    //next();
  } catch (error) {
    return res.status(401).json({ message: "no autorizadooo" });
  }
};

//verifica rol de usuario
exports.verifyToken = async (req, res, next) => {
  try {
    const token = req.headers["x-access-token"];

    if (!token) {
      return res.status(403).json({ message: "Sin token de acceso" });
    }

    const decoded = jwt.verify(token, "aire-api");
    req.userId = decoded.id;

    const user = await Users.findById(req.userId, { password: 0 });
    // console.log(user);
    if (!user) {
      return res.status(404).json({ message: "usuario no encontrado" });
    }

    console.log("token");

    next();
  } catch (error) {
    return res.status(401).json({ message: "no autorizado" });
  }
};

//verifica que el token corresponda al dispositivo
exports.verifyTokenDispositivo = async (req, res, next) => {
  try {
    const token = req.headers["x-access-token"];

    if (!token) {
      return res.status(403).json({ message: "Sin token de acceso" });
    }

    const decoded = jwt.verify(token, "aire-api");

    const dispositivo = await Dispositivos.findById(req.body.dispositivo, {
      password: 0,
    });

    if (!dispositivo) {
      return res
        .status(404)
        .json({ message: "dispositivo no encontrado " + req.body.dispositivo });
    }

    if (decoded.id == req.body.dispositivo) {
      next();
    } else {
      return res.status(404).json({
        message: "dispositivo no coincide con el token",
        token: decoded.id,
        disp: req.body.dispositivo,
      });
    }
    // console.log(user);

    //console.log(decoded);
  } catch (error) {
    return res.status(401).json({ message: "no autorizado" });
  }
};

exports.isAdmin = async (req, res, next) => {
  const user = await Users.findById(req.userId).populate("roles");

  const roles = user.roles;

  for (let i = 0; i < roles.length; i++) {
    if (roles[i].name == "admin") {
      next();
      return;
    }
  }

  return res.status(403).json({ message: "Requiere rol de administrador" });
};

exports.isModerador = async (req, res, next) => {
  const user = await Users.findById(req.userId).populate("roles");

  const roles = user.roles;

  for (let i = 0; i < roles.length; i++) {
    if (roles[i].name == "moderador") {
      next();
      return;
    }
  }

  return res.status(403).json({ message: "Requiere rol de moderador" });
};
