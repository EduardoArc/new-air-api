const Role = require("../models/Role");

exports.checkRolesExisted = async (req, res, next) => {
    
    //const roles = await Role.find({});
    const rolesExistentes = ['admin', 'usuario', 'moderador'];
    if(req.body.roles){

        for(let i = 0; i < req.body.roles.length; i++){
            if(!rolesExistentes.includes(req.body.roles[i])){
                return res.status(400).json({
                    message : `Rol ${req.body.roles[i]} no existe`
                })
            }
        }
    }

    next()
    
};