const Role = require("../models/Role");

module.exports = async () =>{


    
   try {
    const count = await Role.estimatedDocumentCount()

    if(count > 0) return;
    
    const values = await Promise.all([
        new Role({name: 'user', description : "Permiso de lectura"}).save(),
        new Role({name: 'moderador', description : "Permiso de lectura y escritura de usuarios tipo user"}).save(),
        new Role({name: 'admin', description : "Permiso de administrador"}).save()
    ])

    console.log(values);

   } catch (error) {
       console.error(error)
   }

}

