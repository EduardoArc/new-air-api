const Grupos = require("../models/Grupos");
const Organizaciones = require("../models/Organizaciones");
const Users = require("../models/Users");

exports.list = async (req, res) => {
  try {
    const grupos = await Grupos.find({}).populate("organizacion");
    res.json(grupos);
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//crear grupo
exports.add = async (req, res) => {
  const { nombre } = req.body.grupo;
  const { organizacion } = req.body;
  const nuevoGrupo = new Grupos({ nombre, organizacion });

  try {
    await nuevoGrupo.save();

    //asignar grupo a una organizacion
    await Organizaciones.findOneAndUpdate(
      { _id: organizacion },
      {
        $addToSet: {
          grupos: nuevoGrupo._id,
        },
      }
    );

    res.json({ message: "Nuevo grupo agregado", nuevoGrupo });
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//mostrar grupo por id
exports.show = async (req, res) => {
  try {
    const grupo = await Grupos.findById(req.params.id)
      .populate("dispositivos")
      .populate({ path: "users", match: { estado: { $gte: 1 } } });
    if (!grupo) {
      res.status(404).json({ message: "El grupo no existe" });
    }

    res.json(grupo);
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//actualizar grupo
exports.update = async (req, res, next) => {
  try {
    const grupo = await Grupos.findOneAndUpdate(
      { _id: req.params.id },
      { nombre: req.body.nombre },
      { new: true }
    );

    res.json({ message: "Grupo actualizado correctamente" });
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//eliminar grupo
exports.delete = async (req, res, next) => {
  try {
    await Grupos.findOneAndUpdate({ _id: req.params.id }, { estado: 0 });
    res.json({ message: "Grupo deshabilitado correctamente" });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//eliminar grupo
exports.activa = async (req, res, next) => {
  try {
    await Grupos.findOneAndUpdate({ _id: req.params.id }, { estado: 1 });
    res.json({ message: "Grupo activado correctamente" });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//agrega dispositivos al grupo indicado
exports.addDevice = async (req, res) => {
  const { id, dispositivos } = req.body;

  try {
    await Grupos.findOneAndUpdate(
      { _id: id },
      {
        $addToSet: {
          dispositivos: dispositivos,
        },
      }
    );

    res.status(201).json({
      message: "Dispositivos agregados",
    });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

exports.deleteDevice = async (req, res) => {
  const { id, dispositivos } = req.body;

  try {
    await Grupos.findOneAndUpdate(
      { _id: id },
      {
        $pullAll: {
          dispositivos: dispositivos,
        },
      }
    );

    res.status(201).json({
      message: "Dispositivos eliminados",
    });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

exports.gruposByUsuario = async (req, res) => {
  //buscar usuario
  const userId = req.params.id;
  const user = await Users.findById(userId).populate("organizaciones");

  res.status(201).json({
    user,
  });

  //guardar organizaciones del usuario

  //buscar grupos de todas las organizaciones del usuario
};
