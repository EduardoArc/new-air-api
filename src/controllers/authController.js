const Users = require("../models/Users");
const Role = require("../models/Role");
const jwt = require("jsonwebtoken");

exports.signUp = async (req, res) => {
  try {
    const { nombre, email, password, roles } = req.body.user;

    const usuario = req.body.user;

    usuario.password = await Users.encriptPassword(password);

    const user = new Users(usuario);

    if (roles) {
      const foundRoles = await Role.find({ name: { $in: roles } });
      user.roles = foundRoles.map((role) => role._id);
    } else {
      const role = await Role.findOne({ name: "user" });
      user.roles = [role._id];
    }

    await user.save();

    const token = jwt.sign({ id: user._id }, "aire-api", {
      expiresIn: 23486400,
    });

    res.json({ message: "Nuevo usuario agregado", token: token });
  } catch (error) {
    if (error.code == 11000) {
      res.status(400).json({
        message: `Ya existe un usuario con el correo ${req.body.email}`,
      });
    } else {
      res.status(400).json({
        message: "Error al procesar la petición",
        error: error,
      });
    }
  }
};

exports.signIn = async (req, res) => {
  const userFound = await Users.findOne({ email: req.body.email }).populate(
    "roles"
  );

  //const hash = crypto.createHash('sha512').update(req.body.password).digest('hex');
  // return res.json({hash})

  if (!userFound) {
    return res
      .status(400)
      .json({ token: null, message: "Usuario no encontrado" });
  }

  const roles = userFound.roles;

  const user = userFound._id;

  const matchPassword = await Users.comparePassword(
    req.body.password,
    userFound.password
  );

  if (!matchPassword) {
    return res
      .status(401)
      .json({ token: null, message: "Password incorrecta" });
  }

  const token = jwt.sign({ id: userFound._id }, "aire-api", {
    expiresIn: "1h",
  });

  res.json({ token, roles, user });
};
