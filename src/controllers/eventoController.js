const Eventos = require("../models/Eventos");

// listar eventos
exports.list = async (req, res) => {
  try {
    const eventos = await Eventos.find({}).where("estado").equals(1);
    res.json(eventos);
  } catch (error) {
    console.log(error);
    res.send(error);
    next();
  }
};



//obtiene evento según dispositovo
exports.listByDispositivo = async (req, res, next) => {
  try {
  
    const eventos = await Eventos.find({}).where("dispositivo").equals(req.params.dispositivo_id);
    
    if (!eventos) {
      res.status(404).json({ message: "No existen eventos para este dispositivo" });
    }

    res.json(eventos);
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};



