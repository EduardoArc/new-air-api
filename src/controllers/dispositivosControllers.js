const Dispositivos = require("../models/Dispositivos");
const Grupos = require("../models/Grupos");
const Users = require("../models/Users");
const Mediciones = require("../models/Mediciones");
const Organizaciones = require("../models/Organizaciones");
const moment = require("moment");
const jwt = require("jsonwebtoken");
//crear dispositivo
exports.add = async (req, res) => {
  try {
    const { nombre, latitud, longitud, ch_id } = req.body.dispositivo;
    const { grupo } = req.body;

    const nuevoDispositivo = new Dispositivos({
      nombre,
      latitud,
      longitud,
      ch_id,
      grupo,
    });

    await nuevoDispositivo.save();

    const token = jwt.sign({ id: nuevoDispositivo._id }, "aire-api", {
      expiresIn: 126230400000,
    });

    //Agregar token
    await Dispositivos.findOneAndUpdate(
      { _id: nuevoDispositivo._id },
      {
        token: token,
      }
    );

    await Grupos.findOneAndUpdate(
      { _id: grupo },
      {
        $addToSet: {
          dispositivos: nuevoDispositivo,
        },
      }
    );

    res.json({ message: "Nuevo dispositivo agregado" });
  } catch (error) {
    if (error.code == 11000) {
      res.status(400).json({
        message: `Ya existe un dispositivo con el id del canal ${req.body.ch_id}`,
      });
    } else {
      res.status(400).json({
        message: "Error al procesar la petición",
      });
    }
  }
};

// listar dispositivos
exports.list = async (req, res) => {
  try {
    //distinguir tipo de usuario

    const token = req.headers["x-access-token"];
    if (!token) {
      return res.status(403).json({ message: "Sin token de acceso" });
    }
    const decoded = jwt.verify(token, "aire-api");
    const userId = decoded.id;

    const user = await Users.findById(userId).populate("roles");

    if (user.roles[0].name == "admin") {
      const dispositivos = await Dispositivos.find({}).where("estado");
      res.json(dispositivos);
    } else {
      Dispositivos.find({
        grupo: { $in: [user.grupos] },
        estado: 1,
      })
        .populate({
          path: "grupo",
          populate: {
            path: "organizacion",
          },
        })
        .then((dispositivos) => {
          res.json(dispositivos);
        });
    }
  } catch (error) {
    console.log(error);
    res.send(error);
    // next();
  }
};

/*
exports.listMedicion = (req, res) => {
  try {
    Dispositivos.find({})
      .where("estado")
      .equals(1)
      .then((result) => {
        console.log("1");
        let dispositivoMedicion = [];
        result.forEach(async (disp, index) => {
          Mediciones.findOne({})
            .where("dispositivo")
            .equals(disp._id)
            .sort({ _id: -1 })
            .limit(1)
            .then((resp) => {
        console.log("2");

              let objAux = {
                dispositivo: disp,

                medicion: resp,
              };

              dispositivoMedicion.push(objAux);
            });
        });
       
        res.json(dispositivoMedicion);

        console.log("3");
      });
  } catch (error) {
    console.log(error);

    res.send(error);
  }
}*/

exports.listMedicion = async (req, res) => {
  try {
    //res.json("asasdad"); return
    //obtener el id del usuario
    const token = req.headers["x-access-token"];
    if (!token) {
      return res.status(403).json({ message: "Sin token de acceso" });
    }
    const decoded = jwt.verify(token, "aire-api");
    const userId = decoded.id;

    const user = await Users.findById(userId).populate("roles");

    //dispositivos según grupo
    if (user.roles[0].name == "admin") {
      Dispositivos.find({})
        .where("estado")
        .equals(1)
        .populate({
          path: "grupo",
          populate: {
            path: "organizacion",
          },
        })
        .then((dispositivos) => {
          let activos = [];

          dispositivos.forEach((disp, index) => {
            activos.push(disp._id);
          });

          var today = new Date();
          var dd = String(today.getDate()).padStart(2, "0");
          var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
          var yyyy = today.getFullYear();

          today = new Date(yyyy + "/" + mm + "/" + dd);

          var startDate = new Date(
            moment(today)
              .subtract(7, "days")
              .set({ hour: -02, minute: 00, second: 00 })
              .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
          ); //req.params.endTime = 2016-09-25 01:00:00
          var endDate = new Date(
            moment(today)
              .set({ hour: +20, minute: 59, second: 59 })
              .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
          ); //req.params.startTime = 2016-09-25 00:00:00

          Mediciones.find({
            createdAt: {
              $gte: startDate,
              $lt: endDate,
            },
          })
            .where("dispositivo")
            .in(activos)
            .then((mediciones) => {
              respuesta = [];

              dispositivos.forEach((dispositivo) => {
                let mediciones_del_dispositivo = mediciones.filter((medicion) =>
                  medicion.dispositivo.equals(dispositivo._id)
                );
                // console.log(mediciones_del_dispositivo);
                if (mediciones_del_dispositivo.length > 0) {
                  respuesta.push({
                    dispositivo: dispositivo,
                    medicion: mediciones_del_dispositivo.pop(),
                  });
                } else {
                  console.log("No hay");
                }
              });
              res.json(respuesta);
            });
        });
    } else {
      Dispositivos.find({
        grupo: { $in: [user.grupos] },
        estado: 1,
      })
        .populate({
          path: "grupo",
          populate: {
            path: "organizacion",
          },
        })
        .then((dispositivos) => {
          let activos = [];

          dispositivos.forEach((disp, index) => {
            activos.push(disp._id);
          });

          var today = new Date();
          var dd = String(today.getDate()).padStart(2, "0");
          var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
          var yyyy = today.getFullYear();

          today = new Date(yyyy + "/" + mm + "/" + dd);

          var startDate = new Date(
            moment(today)
              .subtract(7, "days")
              .set({ hour: -02, minute: 00, second: 00 })
              .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
          ); //req.params.endTime = 2016-09-25 01:00:00
          var endDate = new Date(
            moment(today)
              .set({ hour: +20, minute: 59, second: 59 })
              .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
          ); //req.params.startTime = 2016-09-25 00:00:00

          Mediciones.find({
            createdAt: {
              $gte: startDate,
              $lt: endDate,
            },
          })
            .where("dispositivo")
            .in(activos)
            .then((mediciones) => {
              respuesta = [];
              dispositivos.forEach((dispositivo) => {
                let mediciones_del_dispositivo = mediciones.filter((medicion) =>
                  medicion.dispositivo.equals(dispositivo._id)
                );
                console.log(mediciones_del_dispositivo);
                if (mediciones_del_dispositivo.length > 0) {
                  respuesta.push({
                    dispositivo: dispositivo,
                    medicion: mediciones_del_dispositivo.pop(),
                  });
                }
              });
              res.json(respuesta);
            });
        });

      /*
      Dispositivos.find(
        {
          grupo: { $in: [user.grupos] },
          estado : 1,
          
        },
        function (err, dispositivos) {


          let activos = [];

          dispositivos.forEach((disp, index) => {
            activos.push(disp._id);
          });
  
          var today = new Date();
          var dd = String(today.getDate()).padStart(2, "0");
          var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
          var yyyy = today.getFullYear();
  
          today = new Date(yyyy + "/" + mm + "/" + dd);
  
          var startDate = new Date(
            moment(today)
              .subtract(7, "days")
              .set({ hour: -02, minute: 00, second: 00 })
              .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
          ); //req.params.endTime = 2016-09-25 01:00:00
          var endDate = new Date(
            moment(today)
              .set({ hour: +20, minute: 59, second: 59 })
              .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
          ); //req.params.startTime = 2016-09-25 00:00:00
  
          Mediciones.find({
            createdAt: {
              $gte: startDate,
              $lt: endDate,
            },
          })
            .where("dispositivo")
            .in(activos)
            .then((mediciones) => {
              respuesta = [];
              dispositivos.forEach((dispositivo) => {
                let mediciones_del_dispositivo = mediciones.filter((medicion) =>
                  medicion.dispositivo.equals(dispositivo._id)
                );
                console.log(mediciones_del_dispositivo);
                respuesta.push({
                  dispositivo: dispositivo,
                  medicion: mediciones_del_dispositivo.pop(),
                });
              });
              res.json(respuesta);
            });



        }
      );
      */
    }

    return;
    //obtener dispositivos del usuario
  } catch (error) {
    console.log(error);
    res.send(error);
  }
};
//Lista a todos los dispositivos con su última medición

/*
exports.listMedicion = async (req, res) =>{
    try{
        let dispositivos = await Dispositivos.find({}).where('estado').equals(1);

        let dispMed = [];

        dispositivos.forEach(async (disp, index) =>{
            const medicion = await Mediciones.findOne({})
            .where("dispositivo")
            .equals(disp._id).sort({_id: -1}).limit(1);
           
            let objAux = {
                dispositivo: disp,
                medicion
            }

            dispMed.push(objAux);

            if(index+1 == dispositivos.length){
                console.log(dispMed);
                res.json(dispMed);
            }
            
        })

        

        
      


    }catch(error){
        console.log(error);
        res.send(error);
        
    }
    
} */

//leer dispositivo por id

exports.show = async (req, res, next) => {
  try {
    const dispositivo = await Dispositivos.findById(req.params.id);

    const grupo = await Grupos.findById(dispositivo.grupo);
    const organizacion = await Organizaciones.findById(grupo.organizacion);

    if (!dispositivo) {
      res.status(404).json({ message: "El dispositivo no existe" });
    }

    res.json({ dispositivo, grupo, organizacion });
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//actualizar dispositivo
exports.update = async (req, res, next) => {
  try {
    const dispositivo = await Dispositivos.findOneAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true }
    );

    res.json({ message: "Dispositivo actualizado correctamente" });
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    await Dispositivos.findOneAndUpdate({ _id: req.params.id }, { estado: 0 });
    res.json({ message: "Dispositivo deshabilitado correctamente" });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

exports.activa = async (req, res, next) => {
  try {
    await Dispositivos.findOneAndUpdate({ _id: req.params.id }, { estado: 1 });
    res.json({ message: "Dispositivo Habilitado correctamente" });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};
