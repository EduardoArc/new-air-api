const Mediciones = require("../models/Mediciones");
const Eventos = require("../models/Eventos");
const axios = require("axios");
const moment = require("moment");
const { google } = require("googleapis");

const momentT = require("moment-timezone");

//crear medicion
const notificar = async (dispositivo, evento) => {
  //validar existencia del dispositivo

  const doc = process.env.GOOGLE_APPLICATION_CREDENTIALS;
  //const doc = "../libs/service-account-file.json";
  //const doc = "/var/service-account-file-google.json"

  const key = require(doc);
  const MESSAGING_SCOPE = "https://www.googleapis.com/auth/firebase.messaging";

  const SCOPES = [MESSAGING_SCOPE];

  const jwtClient = new google.auth.JWT(
    key.client_email,
    null,
    key.private_key,
    SCOPES,
    null
  );

  let token = "";

  jwtClient.authorize(async function (err, tokens) {
    if (err) {
      console.log(err);
      return;
    }

    token = await tokens.access_token;

    let tokenbr = "Bearer " + token;
    // console.log(tokenbr)

    const fcmURL =
      "https://fcm.googleapis.com/v1/projects/nuevo-aire/messages:send";

    let body = {
      message: {
        topic: dispositivo.toString(),
        notification: {
          title: evento.titulo,
          body: evento.mensaje,
        },
        data: {
          story_id: "story_12345",
        },
      },
    };

    await axios
      .post(fcmURL, body, { headers: { Authorization: tokenbr } })
      .then(function (response) {
        // console.log(response);
        console.log("Notificación push de pm25 enviada a los disp suscritos");
      })
      .catch(function (error) {
        console.log(error);
        console.log("Error al enviar notificación push");
      });
  });
  console.log("Notificación enviada a " + dispositivo.toString());
};

exports.add = async (req, res) => {
  try {
    const medicion = new Mediciones(req.body);

    const { rzero, pm10, pm25, co2, temperatura, humedad, dispositivo } =
      req.body;

    if (pm25 >= 80 && pm25 < 110) {
      //verificar si ese día hay un evento para
      console.log("ALERTA");
      var hoy = moment().format("YYYY MM D");
      var today = new Date(hoy);

      const eventoEncontrado = await Eventos.find({
        //query today up to tonight
        createdAt: {
          $gte: today,
        },
      })
        .where("dispositivo")
        .equals(dispositivo);

      //console.log(eventoEncontrado)

      if (eventoEncontrado.length === 0) {
        const evento = new Eventos({
          dispositivo: dispositivo,
          parametro: "pm25",
          alert: true,
          prioridad: 1,
          titulo: "Alerta",
          mensaje:
            "La población de riesgo y los que realizan actividad física intensa, deben limitar los esfuerzos prolongados al aire libre",
        });

        await evento.save();

        notificar(dispositivo, evento);
      }
    }

    if (pm25 >= 110 && pm25 < 169) {
      //verificar si existe una notificación con la prioridad que corresponde a PRE-EMERGENCIA
      console.log("PRE EMERGENCIA");

      var hoy = moment().format("YYYY MM D");
      var today = new Date(hoy);

      const eventoEncontrado = await Eventos.find({
        //query today up to tonight
        createdAt: {
          $gte: today,
        },
        prioridad: {
          $gte: 2,
        },
      })
        .where("dispositivo")
        .equals(dispositivo);

      console.log(eventoEncontrado);

      if (eventoEncontrado.length === 0) {
        const evento = new Eventos({
          dispositivo: dispositivo,
          parametro: "pm25",
          alert: true,
          prioridad: 2,
          titulo: "Pre-emergencia",
          mensaje:
            "La población en general debe limitar el esfuerzo prolongado al aire libre y los vulnerables evitarlo",
        });

        await evento.save();

        notificar(dispositivo, evento);
      }
    }

    if (pm25 >= 169) {
      console.log("EMERGENCIA");

      var hoy = moment().format("YYYY MM D");
      var today = new Date(hoy);

      const eventoEncontrado = await Eventos.find({
        //query today up to tonight
        createdAt: {
          $gte: today,
        },
        prioridad: {
          $gte: 3,
        },
      })
        .where("dispositivo")
        .equals(dispositivo);

      //console.log(eventoEncontrado)

      if (eventoEncontrado.length === 0) {
        const evento = new Eventos({
          dispositivo: dispositivo,
          parametro: "pm25",
          alert: true,
          prioridad: 3,
          titulo: "Emergencia",
          mensaje:
            "La población en general debe suspender los esfuerzos al aire libre",
        });

        await evento.save();

        notificar(dispositivo, evento);
      }
    }

    await medicion.save();
    // global.io.emit("medicion", medicion);

    res.json({ message: "Nueva medicion agregada" });
  } catch (error) {
    res.status(400).json({
      message: "Error al registrar la medicion",
    });
  }
};

// listar mediciones del sipositivo solicitado entre fechas entregadas por params
//definirzonzona horaria

exports.entre = async (req, res) => {
  try {
    var startDate = new Date(
      moment(req.query.start)
        .set({ hour: -02, minute: 00, second: 00 })
        .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
    ); //req.params.startTime = 2016-09-25 00:00:00
    var endDate = new Date(
      moment(req.query.end)
        .set({ hour: +20, minute: 59, second: 59 })
        .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
    ); //req.params.endTime = 2016-09-25 01:00:00

    //  const start = new Date(new Date("2021-11-1").setHours(00, 00, 00));

    const mediciones = await Mediciones.find({
      //query today up to tonight
      createdAt: {
        $gte: startDate,
        $lt: endDate,
      },
    })
      .where("dispositivo")
      .equals(req.params.dispositivo_id);
    console.log(endDate);
    res.send(mediciones);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
};

//promedio de parametros entre las fechas solicitadas
exports.promediodiario = async (req, res) => {
  try {
    var today = new Date();
    //req.query.start

    var startDate = new Date(
      moment(today)
        .set({ hour: -02, minute: 00, second: 00 })
        .utc()
        .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
    ); //req.params.startTime = 2016-09-25 00:00:00
    //  var endDate   =new Date( moment(req.query.end).set({hour:+20,minute:59,second:59}).utc().format("YYYY-MM-DDTHH:mm:ss.SSSZ")); //req.params.endTime = 2016-09-25 01:00:00
    //gmt GMT-3
    //  const start = new Date(new Date("2021-11-1").setHours(00, 00, 00));
    const disp = req.params.dispositivo_id;
    const mediciones = await Mediciones.find({
      //query today up to tonight
      createdAt: {
        $gte: startDate,
        //$lt:  endDate
      },
    })
      .where("dispositivo")
      .equals(disp);

    let promedio = {};
    //rzero
    let rzerosum = 0;
    let rzeronulo = 0;
    //pm25
    let pm25sum = 0;
    let pm25nulo = 0;

    //temperatura
    let temperaturasum = 0;
    let temperaturanulo = 0;

    //humedad
    let humedadsum = 0;
    let humedadnulo = 0;

    let pm10sum = 0;
    let pm10nulo = 0;

    let co2sum = 0;
    let co2nulo = 0;

    mediciones.forEach((medicion) => {
      if (isNaN(medicion.pm10)) {
        pm10nulo++;
      } else {
        pm10sum += medicion.pm10;
      }

      if (isNaN(medicion.co2)) {
        co2nulo++;
      } else {
        co2sum += medicion.co2;
      }

      //rzero
      if (isNaN(medicion.rzero)) {
        rzeronulo++;
      } else {
        rzerosum += medicion.rzero;
      }

      //pm25
      if (isNaN(medicion.pm25)) {
        pm25nulo++;
      } else {
        pm25sum += medicion.pm25;
      }

      //temperatura
      if (isNaN(medicion.temperatura)) {
        temperaturanulo++;
      } else {
        temperaturasum += medicion.temperatura;
      }

      //humedad
      if (isNaN(medicion.humedad)) {
        humedadnulo++;
      } else {
        humedadsum += medicion.humedad;
      }
    });

    let pm10prom = pm10sum / (mediciones.length - pm10nulo);
    let co2prom = co2sum / (mediciones.length - co2nulo);

    //rzero
    let rzeroprom = rzerosum / (mediciones.length - rzeronulo);

    //pm25
    let pm25prom = pm25sum / (mediciones.length - pm25nulo);

    //temperatura
    let temperaturaprom =
      temperaturasum / (mediciones.length - temperaturanulo);

    //humedad
    let humedadprom = humedadsum / (mediciones.length - humedadnulo);

    promedio = {
      rzero: rzeroprom,
      pm10: pm10prom,
      pm25: pm25prom,
      co2: co2prom,
      temperatura: temperaturaprom,
      humedad: humedadprom,
    };

    console.log(promedio);
    res.send(promedio);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
};

//promedio de la fecha solicitada

//promedios de los 7 días anteriores a la fecha
exports.diasanteriores = async (req, res) => {
  try {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = today.getFullYear();

    today = new Date(yyyy + "/" + mm + "/" + dd);

    var startDate = new Date(
      moment(today).subtract(7, "days").format("YYYY-MM-DDTHH:mm:ss.SSSZ")
    ); //req.params.endTime = 2016-09-25 01:00:00
    var endDate = new Date(
      moment(today)
        .set({ hour: +24, minute: 00, second: 00 })
        .format("YYYY-MM-DDTHH:mm:ss.SSSZ")
    ); //req.params.startTime = 2016-09-25 00:00:00

    //  const start = new Date(new Date("2021-11-1").setHours(00, 00, 00));

    const mediciones = await Mediciones.find({
      //query today up to tonight
      createdAt: {
        $gte: startDate,
        $lt: endDate,
      },
    })
      .where("dispositivo")
      .equals(req.params.dispositivo_id);
    //  console.log(endDate);

    //Mediciones obtiene 7 días, nitamos para obtener un promedio por día

    //4 dias

    mediciones.forEach((medicion, index) => {
      let med = new Date(medicion.createdAt);
      let str = String(med);
      let dia = str.split(" ")[2];

      if (parseInt(dia) != parseInt(dia) + 1) {
        // console.log("diferente")
        //console.log( (index+1) +" : "+ ( parseInt(dia) +1));
      }
    });

    res.send(mediciones);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
};

//promedio entre los dispositivos solicitados
/*
let fields = [];
for (const [key, value] of Object.entries(medicion)) {
  fields.push({"field" : key , "valor" : value})
  
}
console.log(fields)*/

//leer medicion por id
exports.show = async (req, res, next) => {
  try {
    const medicion = await Mediciones.findById(req.params.id);
    if (!medicion) {
      res.status(404).json({ message: "La medicion no existe" });
    }

    let fields = [
      { field: "rzero", value: medicion.rzero },
      { field: "PM 10", value: medicion.pm10 },
      { field: "PM 2.5", value: medicion.pm25 },
    ];

    /*
    for (var key in medicion) {
      console.log(key)
     // fields.push({"field" : key , "valor" : value})
      
    }
    //console.log(fields)*/

    res.json(fields);
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//últimas 5 mediciones
exports.lastFiveData = async (req, res, next) => {
  try {
    const disp = req.params.dispositivo_id;

    const mediciones = await Mediciones.find({})
      .where("dispositivo")
      .equals(disp)
      .sort({ _id: -1 })
      .limit(5);

    const formateadas = mediciones.map((medicion) => {
      return {
        id: medicion._id,
        co2: medicion.co2,
        pm25: medicion.pm25,
        pm10: medicion.pm10,
        temperatura: medicion.temperatura,
        // hora: moment(medicion.createdAt).format("HH:mm:ss"),
        hora: momentT
          .tz(medicion.createdAt, "America/Santiago")
          .format("HH:mm:ss"),
      };
    });

    res.json(formateadas);
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//obtiene mediciones promediadas de los 7 días anteriores según dispositivo
exports.weekendAverageByDevice = async (req, res, next) => {
  try {
    //cuenta 7 días atrás a partir del día actual
    var startDate = moment({ H: 0, m: 0 }).subtract(7, "days").format();
    //req.params.endTime = 2016-09-25 01:00:00
    console.log(startDate);
    //Día actual
    var endDate = moment({ H: 23, m: 59 }).format();

    //req.query.start

    //var startDate = new Date( moment(today).set({hour:-02,minute:00,second:00}).utc().format("YYYY-MM-DDTHH:mm:ss.SSSZ")); //req.params.startTime = 2016-09-25 00:00:00

    const disp = req.params.dispositivo_id;

    const mediciones = await Mediciones.find({
      //query today up to tonight
      createdAt: {
        $gte: startDate,
        $lt: endDate,
      },
    })
      .where("dispositivo")
      .equals(disp);

    var dateArr = {}; //Array where rest of the dates will be stored

    const prevDate = moment().subtract(7, "days"); //7 days back date from today(This is the from date)

    const nextDate = moment().add(1, "days"); //Date after 7 days from today (This is the end date)

    //extracting date from objects in MM-DD-YYYY format
    const comienza = moment(prevDate._d).format("MM-DD-YYYY");
    const termina = moment(nextDate._d).format("MM-DD-YYYY");

    //creating JS date objects
    var start = new Date(comienza);
    var end = new Date(termina);

    //Logic for getting rest of the dates between two dates("FromDate" to "EndDate")
    while (start < end) {
      dateArr[moment(start).format("YYYY-MM-DD")] = {
        num: 0,
        name: moment(start).format("YYYY-MM-DD"),
        total_co2: 0,
        total_pm25: 0,
        total_pm10: 0,
        total_temperatura: 0,
        co2: 0,
        pm25: 0,
        pm10: 0,
        temperatura: 0,
      };

      var newDate = start.setDate(start.getDate() + 1);
      start = new Date(newDate);
    }

    //console.log('Dates:- ');
    //console.log(dateArr);
    if (!mediciones) {
      res.status(404).json({ message: "La medicion no existe" });
    }

    mediciones.forEach((medicion, index) => {
      //console.log(medicion.createdAt)

      //let fecha = medicion.createdAt.toISOString().split("T")[0];

      let fecha = moment(medicion.createdAt).format().split("T")[0];

      if (dateArr[fecha] == undefined) {
        console.log(medicion);
      } else {
        //  console.log(dateArr[fecha]);
        dateArr[fecha].num += 1;

        dateArr[fecha].total_co2 += medicion.co2;
        dateArr[fecha].co2 = Math.round(
          dateArr[fecha].total_co2 / dateArr[fecha].num
        );

        dateArr[fecha].total_pm25 += medicion.pm25;
        dateArr[fecha].pm25 = Math.round(
          dateArr[fecha].total_pm25 / dateArr[fecha].num
        );

        dateArr[fecha].total_pm10 += medicion.pm10;
        dateArr[fecha].pm10 = Math.round(
          dateArr[fecha].total_pm10 / dateArr[fecha].num
        );

        dateArr[fecha].total_temperatura += medicion.temperatura;
        dateArr[fecha].temperatura = Math.round(
          dateArr[fecha].total_temperatura / dateArr[fecha].num
        );
      }
    });
    arr = [];

    for (const dia in dateArr) {
      arr.push(dateArr[dia]);
    }

    res.json(arr);
    //res.json({"Hola": "Holi"})
    //console.log(dateArr);
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//Trae última medición del dispositivo
exports.ultimaMedicion = async (req, res, next) => {
  try {
    const id = req.params.dispositivo_id;
    // const medicion = await Mediciones.findById(id).sort({_id: -1}).limit(1);

    const medicion = await Mediciones.findOne({})
      .where("dispositivo")
      .equals(req.params.dispositivo_id)
      .sort({ _id: -1 })
      .limit(1);

    res.json({ medicion });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    await Mediciones.findOneAndUpdate({ _id: req.params.id }, { estado: 0 });
    res.json({ message: "Medicion deshabilitada correctamente" });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};
