const Organizaciones = require("../models/Organizaciones");

exports.list = async (req, res) => {
  try {
    const organizaciones = await Organizaciones.find({});
    res.json(organizaciones);

    //Buscar el usuario por token
    //Buscar sus organizaciones
    //Listar las organizaciones del usuario obtenido

    /*
       model.find({
    '_id': { $in: [
       '4ed3ede8844f0f351100000c','4ed3f117a844e0471100000d' 
     
    ]}
}, function(err, docs){
     console.log(docs);
});
       */
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//crear grupo
exports.add = async (req, res) => {
  const organizacion = new Organizaciones(req.body);
  try {
    await organizacion.save();
    res.json({ message: "Nueva organización agregada" });
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//mostrar organizacion por id
exports.show = async (req, res) => {
  try {
    const organizacion = await Organizaciones.findById(req.params.id).populate({
      path: "grupos",
      match: { estado: { $gte: 1 } },
    });
    if (!organizacion) {
      res.status(404).json({ message: "la Organizacion no existe" });
    }

    res.json(organizacion);
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//actualizar organizacion
exports.update = async (req, res, next) => {
  try {
    await Organizaciones.findOneAndUpdate({ _id: req.params.id }, req.body, {
      new: true,
    });

    res.json({ message: "Organización actualizada correctamente" });
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    await Organizaciones.findOneAndUpdate(
      { _id: req.params.id },
      { estado: 0 }
    );
    res.json({ message: "Organización deshabilitada correctamente" });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

exports.activa = async (req, res, next) => {
  try {
    await Organizaciones.findOneAndUpdate(
      { _id: req.params.id },
      { estado: 1 }
    );
    res.json({ message: "Organización habilitada correctamente" });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//agrega uno o muchos grupos

exports.addGrupos = async (req, res) => {
  const { id, grupos } = req.body;

  try {
    await Organizaciones.findOneAndUpdate(
      { _id: id },
      {
        $addToSet: {
          grupos: grupos,
        },
      }
    );

    res.status(201).json({
      message: "Grupos agregados",
    });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

exports.deleteGrupos = async (req, res) => {
  const { id, grupos } = req.body;

  try {
    await Organizaciones.findOneAndUpdate(
      { _id: id },
      {
        $pullAll: {
          grupos: grupos,
        },
      }
    );

    res.status(201).json({
      message: "Grupos eliminados",
    });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};
