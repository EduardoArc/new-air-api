const Users = require("../models/Users");
const Grupos = require("../models/Grupos");
const Organizaciones = require("../models/Organizaciones");
const Role = require("../models/Role");
const jwt = require("jsonwebtoken");
const Dispositivos = require("../models/Dispositivos");

//crear usuario :add

exports.add = async (req, res) => {
  try {
    const { nombre, email, password, roles } = req.body.user;
    const { organizacion, grupo } = req.body;

    const usuario = req.body.user;
    if (password) {
      usuario.password = await Users.encriptPassword(password);
    }

    const user = new Users(usuario);

    if (roles) {
      const foundRoles = await Role.find({ name: { $in: roles } });
      user.roles = foundRoles.map((role) => role._id);
    } else {
      const role = await Role.findOne({ name: "user" });
      user.roles = [role._id];
    }

    const newUser = await user.save();

    //relacionar usuario con su organizacion
    await Organizaciones.findOneAndUpdate(
      { _id: newUser._id },
      {
        $addToSet: {
          organizaciones: organizacion,
        },
      }
    );

    //relacionar usuario con organizacion
    await Users.findOneAndUpdate(
      { _id: newUser._id },
      {
        $addToSet: {
          organizaciones: organizacion,
        },
      }
    );

    //relacionar grupo con usuario
    await Grupos.findOneAndUpdate(
      { _id: grupo },
      {
        $addToSet: {
          users: newUser._id,
        },
      }
    );
    //relacionar al usuario con el grupo
    await Users.findOneAndUpdate(
      { _id: newUser._id },
      {
        $addToSet: {
          grupos: grupo,
        },
      }
    );

    const token = jwt.sign({ id: user._id }, "aire-api", { expiresIn: 86400 });

    res.json({ message: "Nuevo usuario agregado", token: token });
  } catch (error) {
    if (error.code == 11000) {
      res.status(400).json({
        message: `Ya existe un usuario con el correo ${req.body.user.email}`,
      });
    } else {
      res.status(400).json({
        message: "Error al procesar la petición",
        error: error,
      });
    }
  }
};

// primera acción :list
exports.list = async (req, res) => {
  try {
    const users = await Users.find({}).populate("roles");

    res.json(users);
  } catch (error) {
    console.log(error);
    res.send(error);
    next();
  }
};

exports.getAlumnosPorGrupo = async (req, res) => {
  const grupo = await Grupos.findById(req.params.id).populate({
    path: "users",
    select: "nombre",
    match: { $and: [{ estado: { $gte: 1 } }] },
    populate: {
      path: "roles",
      options: { limit: 2 },
    },
  });

  if (!grupo) {
    res.status(404).json({ message: "El grupo no existe" });
  }

  res.json(grupo.users);
};

// Listado de alumnos según su la organiación del moderador:list
exports.getAlumnos = async (req, res) => {
  try {
    //obtener el id del moderador del token
    const token = req.headers["x-access-token"];
    const decoded = jwt.verify(token, "aire-api");
    const userId = decoded.id;

    let usuario = await Users.findById(userId).populate({
      path: "organizaciones",
      match: { estado: { $gte: 1 } },
      populate: {
        path: "users",
        match: { estado: { $gte: 1 } },
      },
    });

    const orgs = usuario.organiozaciones;
    res.json(typeof usuario.organiozaciones);
    return;
    let organizacionesEncontradas = [];
    usuario.organiozaciones.forEach((org) => {
      organizacionesEncontradas.push(org);
    });

    res.json(organizacionesEncontradas);
    return;

    res.json(users);
  } catch (error) {
    console.log(error);
    res.send(error);
  }
};

//muestra un usuario según su id :show
exports.show = async (req, res) => {
  try {
    const user = await Users.findById(req.params.id)
      .populate("organizaciones")
      .populate("roles");
    if (!user) {
      res.status(404).json({ message: "El usuario no existe" });
    }
    res.json(user);
  } catch (error) {
    console.log(error);
    console.log(error.stack);
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//actualiza a un usuario
exports.update = async (req, res) => {
  try {
    await Users.findOneAndUpdate(
      { _id: req.params.id },
      {
        nombre: req.body.nombre,
        email: req.body.email,
      }
    );
    res.json({ message: "usuario modificado correctamente" });
  } catch (error) {
    res.status(400).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//elimina un usuario
exports.delete = async (req, res, next) => {
  try {
    await Users.findOneAndUpdate({ _id: req.params.id }, { estado: 0 });
    res.json({ message: "Usuario deshabilitado correctamente" });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//activa un usuario
exports.activa = async (req, res, next) => {
  try {
    await Users.findOneAndUpdate({ _id: req.params.id }, { estado: 1 });
    res.json({ message: "Usuario Habilitado correctamente" });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

//agrega una o muchas organizaciones

exports.addOrganizacion = async (req, res) => {
  const { id, organizaciones } = req.body;

  try {
    await Users.findOneAndUpdate(
      { _id: id },
      {
        $addToSet: {
          organizaciones: organizaciones,
        },
      }
    );

    res.status(201).json({
      message: "Organizaciones agregadas",
    });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

exports.deleteOrganiacion = async (req, res) => {
  const { id, organizaciones } = req.body;
  console.log(organizaciones);
  try {
    await Users.findOneAndUpdate(
      { _id: id },
      {
        $pullAll: {
          organizaciones: organizaciones,
        },
      }
    );

    res.status(201).json({
      message: "Organizaciones eliminadas",
    });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};

exports.getGrupos = async (req, res) => {
  //buscar usuario
  const userId = req.params.id;
  const user = await Users.findById(userId);

  const grupos = await Grupos.find({
    organizacion: { $in: user.organizaciones },
  })
    .populate({
      path: "organizacion",
      select: "nombre",
      match: { estado: { $gte: 1 } },
    })
    .where("estado")
    .equals(1);

  res.status(201).json({
    grupos,
  });
};

exports.usuarioDispositivos = async (req, res) => {
  const user = await Users.findById(req.params.id);

  const grupos = await Grupos.find({
    organizacion: { $in: user.organizaciones },
  })
    .select("_id")
    .where("estado")
    .equals(1);

  let grupoIds = [];
  grupos.forEach((item) => {
    grupoIds.push(item._id);
  });

  const dispositivos = await Dispositivos.find({
    grupo: { $in: grupoIds },
  });

  res.status(201).json({
    dispositivos,
  });
};

exports.changePassword = async (req, res) => {
  const { user, antigua, password, confirm } = req.body;

  const userFound = await Users.findById(user);

  if (!userFound) {
    return res.status(400).json({ message: "Usuario no encontrado" });
  }

  const matchPassword = await Users.comparePassword(
    antigua,
    userFound.password
  );

  if (!matchPassword) {
    return res.status(403).json({ message: "Password incorrecta" });
  } else {
    //volver a encriptar nueva password

    await Users.findOneAndUpdate(
      { _id: user },
      {
        password: await Users.encriptPassword(password),
        temp_password: "segura",
      }
    );
    res.json({ message: "usuario modificado correctamente" });
  }
};

/*
//agrega uno o muchos grupos

exports.addGrupo = async (req, res) => {
  const { id, grupos } = req.body;

  try {
    
     await  Users.findOneAndUpdate(
        { _id: id },
        {
          $addToSet: {
            grupos: grupos
          },
        }
      );

    res.status(201).json({
      message: "Grupos agregados",
    });
  } catch (error) {
    res.status(401).json({
      message: "Error al procesar la petición",
      error: error,
    });
  }
};



exports.deleteGrupo = async (req, res) => {
    const { id, grupos } = req.body;

    try {
      
       await  Users.findOneAndUpdate(
          { _id: id },
          {
            $pullAll: {
              grupos: grupos
            },
          }
        );
  
      res.status(201).json({
        message: "Grupos eliminados",
      });

      
    } catch (error) {
      res.status(401).json({
        message: "Error al procesar la petición",
        error: error,
      });
    }
};*/
